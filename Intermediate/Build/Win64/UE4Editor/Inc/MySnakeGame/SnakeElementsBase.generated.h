// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef MYSNAKEGAME_SnakeElementsBase_generated_h
#error "SnakeElementsBase.generated.h already included, missing '#pragma once' in SnakeElementsBase.h"
#endif
#define MYSNAKEGAME_SnakeElementsBase_generated_h

#define MySnakeGame_Source_MySnakeGame_SnakeElementsBase_h_16_SPARSE_DATA
#define MySnakeGame_Source_MySnakeGame_SnakeElementsBase_h_16_RPC_WRAPPERS \
	virtual void SetFirstElementType_Implementation(); \
 \
	DECLARE_FUNCTION(execToggleCollision); \
	DECLARE_FUNCTION(execHandleBeginOverlap); \
	DECLARE_FUNCTION(execSetFirstElementType);


#define MySnakeGame_Source_MySnakeGame_SnakeElementsBase_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execToggleCollision); \
	DECLARE_FUNCTION(execHandleBeginOverlap); \
	DECLARE_FUNCTION(execSetFirstElementType);


#define MySnakeGame_Source_MySnakeGame_SnakeElementsBase_h_16_EVENT_PARMS
#define MySnakeGame_Source_MySnakeGame_SnakeElementsBase_h_16_CALLBACK_WRAPPERS
#define MySnakeGame_Source_MySnakeGame_SnakeElementsBase_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASnakeElementsBase(); \
	friend struct Z_Construct_UClass_ASnakeElementsBase_Statics; \
public: \
	DECLARE_CLASS(ASnakeElementsBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MySnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ASnakeElementsBase) \
	virtual UObject* _getUObject() const override { return const_cast<ASnakeElementsBase*>(this); }


#define MySnakeGame_Source_MySnakeGame_SnakeElementsBase_h_16_INCLASS \
private: \
	static void StaticRegisterNativesASnakeElementsBase(); \
	friend struct Z_Construct_UClass_ASnakeElementsBase_Statics; \
public: \
	DECLARE_CLASS(ASnakeElementsBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MySnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ASnakeElementsBase) \
	virtual UObject* _getUObject() const override { return const_cast<ASnakeElementsBase*>(this); }


#define MySnakeGame_Source_MySnakeGame_SnakeElementsBase_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeElementsBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeElementsBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeElementsBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeElementsBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeElementsBase(ASnakeElementsBase&&); \
	NO_API ASnakeElementsBase(const ASnakeElementsBase&); \
public:


#define MySnakeGame_Source_MySnakeGame_SnakeElementsBase_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeElementsBase(ASnakeElementsBase&&); \
	NO_API ASnakeElementsBase(const ASnakeElementsBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeElementsBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeElementsBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASnakeElementsBase)


#define MySnakeGame_Source_MySnakeGame_SnakeElementsBase_h_16_PRIVATE_PROPERTY_OFFSET
#define MySnakeGame_Source_MySnakeGame_SnakeElementsBase_h_13_PROLOG \
	MySnakeGame_Source_MySnakeGame_SnakeElementsBase_h_16_EVENT_PARMS


#define MySnakeGame_Source_MySnakeGame_SnakeElementsBase_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MySnakeGame_Source_MySnakeGame_SnakeElementsBase_h_16_PRIVATE_PROPERTY_OFFSET \
	MySnakeGame_Source_MySnakeGame_SnakeElementsBase_h_16_SPARSE_DATA \
	MySnakeGame_Source_MySnakeGame_SnakeElementsBase_h_16_RPC_WRAPPERS \
	MySnakeGame_Source_MySnakeGame_SnakeElementsBase_h_16_CALLBACK_WRAPPERS \
	MySnakeGame_Source_MySnakeGame_SnakeElementsBase_h_16_INCLASS \
	MySnakeGame_Source_MySnakeGame_SnakeElementsBase_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MySnakeGame_Source_MySnakeGame_SnakeElementsBase_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MySnakeGame_Source_MySnakeGame_SnakeElementsBase_h_16_PRIVATE_PROPERTY_OFFSET \
	MySnakeGame_Source_MySnakeGame_SnakeElementsBase_h_16_SPARSE_DATA \
	MySnakeGame_Source_MySnakeGame_SnakeElementsBase_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	MySnakeGame_Source_MySnakeGame_SnakeElementsBase_h_16_CALLBACK_WRAPPERS \
	MySnakeGame_Source_MySnakeGame_SnakeElementsBase_h_16_INCLASS_NO_PURE_DECLS \
	MySnakeGame_Source_MySnakeGame_SnakeElementsBase_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MYSNAKEGAME_API UClass* StaticClass<class ASnakeElementsBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MySnakeGame_Source_MySnakeGame_SnakeElementsBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
