// Copyright Epic Games, Inc. All Rights Reserved.

#include "MySnakeGame.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, MySnakeGame, "MySnakeGame" );
