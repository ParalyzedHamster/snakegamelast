// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MySnakeGameGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class MYSNAKEGAME_API AMySnakeGameGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
