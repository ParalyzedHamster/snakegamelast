// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawnBase.generated.h"

class UCameraComponent;
class ASnakeBase;
class AApple;
UCLASS()
class MYSNAKEGAME_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();

	UPROPERTY(BlueprintReadWrite)
		float MinX = -550.f;
	UPROPERTY(BlueprintReadWrite)
		float MaxX = 550.f; 
	UPROPERTY(BlueprintReadWrite)
		float MinY = -850.f; 
	UPROPERTY(BlueprintReadWrite)
		float MaxY = 850.f;
	UPROPERTY(BlueprintReadWrite)
		float SpawnZ =0.f;

	float StepDeley = 1.f;
	float BuferTime = 0.f;
	float DeltaSpawn = 60.f;

	UPROPERTY(BlueprintReadWrite)
	UCameraComponent* PawnCamera;

	UPROPERTY(BlueprintReadWrite)
	ASnakeBase* SnakeActor;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeBase> SnakeActorClass;

	UPROPERTY(BlueprintReadWrite)
	AApple* AppleActor;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AApple> AppleActorClass;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void CreateSnakeActor();
	void AddRandonLocationFood();
	void TimeAppleSpawn();
	UFUNCTION()
		void HendlePlayerVerticalInput(float value);
	UFUNCTION()
		void HendlePlayerHorizontalInput(float value);
};
